fork profiler
================

**fork profiler** 可以列举所有产生fork的PC。可以帮助识别哪些指令导致路径爆炸。

# 示例

执行命令如下：

```
    `forkprofiler -trace=ExecutionTrace.dat -outputdir=output -moddir=moddir` 
```

forkprofiler 将生成两个结果文件：forkprofile.txt和statetree.dot.

其中statetree.dot表示当前trace文件记录下的fork情况，包括PC、state关系。如下图：

![statetree](imgs/statetree.png)
图 statetree

forkprofile.txt记录了fork的PC，模块、以及fork的次数。下面是测试astdrive.sys的记录：

```
      1 #Pc         Module  ForkCnt Source  Function    Line                                                               
      2 0x004a38ce  ntoskrnl.exe    1   ?   ?   0                                                                          
      3 0x004a38df  ntoskrnl.exe    2   ?   ?   0                                                                          
      4 0x004a3b5c  ntoskrnl.exe    2   ?   ?   0                                                                          
      5 0x004a3b65  ntoskrnl.exe    2   ?   ?   0                                                                          
      6 0x004a3930  ntoskrnl.exe    3   ?   ?   0                                                                          
      7 0x00011147  astdrive.sys    5   ?   ?   0                                                                          
      8 0x00011150  astdrive.sys    5   ?   ?   0                                                                          
      9 0x00011159  astdrive.sys    5   ?   ?   0                                                                          
     10 0x004a3a2b  ntoskrnl.exe    6   ?   ?   0                                                                          
     11 0x004a3a7f  ntoskrnl.exe    6   ?   ?   0   
```


# 依赖的插件

* ExecutionTracer


# 可选插件

* ModuleTracer (_用于调试信息_)



