tbtrace
================

** tbtrace ** 可以分析符号执行过程中的多条路径，包括执行的bb块的地址、执行该处的寄存器信息、内存访问信息等，并结合IDA的反汇编信息给出反汇编的指令信息。

# 示例

执行命令如下：

```
    `tbtrace -printDisassembly -printMemory -printRegisters -pathId=0 -trace=ExecutionTrace.dat -outputdir=output -moddir=moddir` 
```

上面的命令将对path0进行分析，在输出目录下生成0.txt，部分信息如下：

### 反汇编信息：

```
ESC[1;33m.text:00011210 ; int __stdcall sub_11210(PDRIVER_OBJECT        DriverObject, int)^
ESC[1;33m.text:00011210 sub_11210       proc near              ; CODE XREF: DriverEntry+42^Yj^
ESC[1;33m.text:00011210^
ESC[1;33m.text:00011210 DestinationString= UNICODE_STRING ptr -18h^
ESC[1;33m.text:00011210 SymbolicLinkName= UNICODE_STRING        ptr -10h^
ESC[1;33m.text:00011210 var_8          = dword ptr -8^
ESC[1;33m.text:00011210 DeviceObject    = dword ptr -4^
ESC[1;33m.text:00011210 DriverObject    = dword ptr  8^
ESC[1;33m.text:00011210^
ESC[1;33m.text:00011210                mov     edi, edi^
ESC[1;33m.text:00011212                push    ebp^
ESC[1;33m.text:00011213                mov     ebp, esp^
ESC[1;33m.text:00011215                sub     esp, 18h^
ESC[1;33m.text:00011218                mov     eax, [ebp+DriverObject]^
ESC[1;33m.text:0001121B                mov     dword ptr [eax+34h], offset sub_11040^
ESC[1;33m.text:00011222                mov     ecx, [ebp+DriverObject]^
ESC[1;33m.text:00011225                mov     dword ptr [ecx+38h], offset sub_11090^
ESC[1;33m.text:0001122C                mov     edx, [ebp+DriverObject]^
ESC[1;33m.text:0001122F                mov     dword ptr [edx+40h], offset sub_11090^
ESC[1;33m.text:00011236                mov     eax, [ebp+DriverObject]^
ESC[1;33m.text:00011239                mov     dword ptr [eax+70h], offset dispatch_main^
ESC[1;33m.text:00011240                push    offset aDeviceAstdrive ; "\\Device\\ASTDrivers"^
ESC[1;33m.text:00011245                lea     ecx, [ebp+DestinationString]^
ESC[1;33m.text:00011248                push    ecx             ; DestinationString^
ESC[1;33m.text:00011249                call    ds:RtlInitUnicodeString^

```

### 寄存器信息：

```
0x804da2a7 - 
    EAX: 0x555555558178a788 ECX: 0x55555555f9e93c64 EDX: 0x555555558178a788 EBX: 0x5555555500000000 ESP: 0x55555555f9e9
3c58 EBP: 0x55555555f9e93c7c ESI: 0x55555555e16dd11a EDI: 0x555555558178a788 
    (ntoskrnl.exe 0x4022a7)

```

### 内存访问信息：

```
S=0 P=0x39000 PC=0x804da2a7 ---W4[0xf9e93c54]=0x008178a788      (ntoskrnl.exe 0x4022a7)
S=0 P=0x39000 PC=0x804da2a8 ---R4[0xf9e93c60]=0x00f9de51b0      (ntoskrnl.exe 0x4022a8)
S=0 P=0x39000 PC=0x804da2ac ---R4[0xf9e93c5c]=0x00f9e93c64      (ntoskrnl.exe 0x4022ac)
S=0 P=0x39000 PC=0x804da2b0 ---W4[0xf9e93c64]=0x0000000000      (ntoskrnl.exe 0x4022b0)
S=0 P=0x39000 PC=0x804da2b6 ---W4[0xf9e93c68]=0x00f9de51b0      (ntoskrnl.exe 0x4022b6)

```

# 依赖的插件

* ExecutionTracer
* MemoryTracer
* MemoryChecker


# 可选插件

* ModuleTracer (_用于调试信息_)




