Toos of S2E
================

## 基本分析

S2E提供的工具大部分用于trace的分析。可参考[revnic][1]

Trace分析都基于LogParser类和PathBuilder类实现。

* LogParser基于LogEvent，通过发送一个信号来实现其他分析。

* PathBuilder主要把trace文件根据fork的地址构建成一颗ExecutionTree，用于后续的路径分析。

S2E可以trace的数据在TraceEntries.h中，包括：

```
enum ExecTraceEntryType {
    TRACE_MOD_LOAD = 0,    //模块加载，模块名称、基址、加载地址、大小
    TRACE_MOD_UNLOAD,       //模块卸载，加载地址
    TRACE_PROC_UNLOAD,      //进程退出？没实现
    TRACE_CALL,             //函数调用，调用和被调地址    
    TRACE_RET,              //函数返回
    TRACE_TB_START,         //翻译块开始
    TRACE_TB_END,           //翻译块结束
    TRACE_MODULE_DESC,      //模块描述
    TRACE_FORK,             //fork信息，pc，statecount，children[1]。为什么是1个？
    TRACE_CACHESIM,         //    
    TRACE_TESTCASE,         //测试用例信息
    TRACE_BRANCHCOV,        //分支覆盖    
    TRACE_MEMORY,           //内存访问，pc，地址，值，大小，标识，hostAddress，concreteBuffer
    TRACE_PAGEFAULT,        //缺页异常    
    TRACE_TLBMISS,          //tlb miss
    TRACE_ICOUNT,           //当前已经执行的执行数
    TRACE_MEM_CHECKER,      //将内存的起始地址、大小、flag以及名称进行序列化
    TRACE_EXCEPTION,        //异常的PC和vector？
    TRACE_STATE_SWITCH,     //切换到新的state
    TRACE_MAX
};
```

## 基本使用

S2E中目前包括7个工具：cacheprof  coverage  debugger  forkprofiler  icounter  s2etools-config  tbtrace

其中常用的工具包括：


* [forkprofiler](forkprofiler.md)

* [coverage](coverage.md)

* [tbtrace](tbtrace.md)

使用这些工具之前需要生成测试目标的IDA分析文件放置于moddir目录下，包括bblist, fcn, lst, asm 文件。通过调用S2E提供的两个python脚本即可。分析pcntpci5.sys时生成的文件如下：

```
     pcntpci5.sys
     pcntpci5.sys.asm
     pcntpci5.sys.bblist
     pcntpci5.sys.fcn
     pcntpci5.sys.lst
```


## 参考文献

[1] Reverse engineering of binary device drivers with RevNIC




-------------

[1]: http://google.com/ "Reverse engineering of binary device drivers with RevNIC"
