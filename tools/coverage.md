coverage
================

** coverage ** 可以分析符号执行测试中的覆盖率信息。可以产生3个结果文件：.bbcov, .timecov 和 .repcov


# 示例

执行命令如下：

```
    `coverage -trace=ExecutionTrace.dat -outputdir=output -moddir=moddir` 
```

coverage 将生成3个结果文件： astdrive.sys.bbcov,astdrive.sys.timecov和astdrive.sys.repcov.

### bbcov文件

其中astdrive.sys.bbcov包含覆盖过的bb块，其生成的信息如下：

```
  1 -0x000119e0:0x000119e5                                                                                             
  2  
  3 +0x00014005:0x00014017
  4 +0x00014018:0x0001401b
  5 +0x0001401c:0x00014037
  6 -0x00014038:0x0001403e
  7 +0x0001403f:0x0001404b
  8  
  9 +0x000110d0:0x00011101
 10 -0x00011102:0x0001110c
 11 -0x0001110d:0x00011116
 12 +0x00011117:0x00011148
....

```
以+0x00014005:0x00014017为例，+号表示执行过（-号表示未执行过），0x00014005为基本块的起始地址，0x00014017为基本块的结束地址。

### timecov文件

猜测应该是基本块执行的时间，其生成的结果信息如下：
```
  1 0 0  0x14005 0x14017                                                                                               
  2 0 1  0x14018 0x1401b
  3 0 2  0x1401c 0x14037
  4 0 3  0x1403f 0x1404b
  5 0 4  0x11210 0x1124e
  6 0 5  0x1124f 0x1125d
  7 0 6  0x1125e 0x11277
  8 0 7  0x11278 0x11280
  9 0 8  0x11281 0x1128e
 10 0 9  0x1128f 0x11297
 11 0 10  0x112a2 0x112aa
 12 46 11  0x11090 0x110bc
 13 46 12  0x110bd 0x110c4
 14 47 13  0x110d0 0x11101
 15 47 14  0x11117 0x11148
 16 47 15  0x11149 0x11151
 17 47 16  0x11152 0x1115a
 18 47 17  0x1115b 0x1115c
 19 47 18  0x111a4 0x111a6
 20 47 19  0x111a7 0x111ac
 21 64 20  0x11189 0x11195
 22 64 21  0x11810 0x11821
 23 64 22  0x11822 0x11828
 24 64 23  0x11829 0x11832
 25 64 24  0x119d5 0x119da

```
其中第一列应该是执行的时间，第二列表示序号，后两列表示基本块的起始和结束地址。

### repcov

对覆盖率的统计，其信息如下：
```
  1 (  0%) 000/001 DbgPrint                                           The function was not exercised                   
  2 ( 80%) 004/005 DriverEntry                                        0x14038
  3 ( 81%) 013/016 dispatch_main                                      0x11102 0x1110d 0x11184
  4 (  0%) 000/006 sub_11040                                          The function was not exercised
  5 (100%) 002/002 sub_11090                                          Full coverage
  6 ( 87%) 007/008 sub_11210                                          0x11298
  7 ( 13%) 004/030 sub_112B0                                          0x112cb 0x112dc 0x112e3 0x112ed 0x112f4 0x11305 0
  8 (  0%) 000/008 sub_114E0                                          The function was not exercised
  9 (  0%) 000/011 sub_115A0                                          The function was not exercised
 10 (  3%) 001/026 sub_11690                                          0x116a7 0x116b1 0x116bb 0x116c2 0x116d3 0x116da 0
 11 ( 11%) 004/035 sub_11810                                          0x11833 0x11849 0x11853 0x11867 0x1186e 0x11878 0
 12  
 13 Basic block coverage:    35/148(23%)
 14 Function block coverage: 35/122(28%)
 15 Total touched functions: 7/11(63%)
 16 Fully covered functions: 1/11(9%)
 17 Time to cover last block: 114
 18 # paths:                  38
~                                               
```


# 依赖的插件

* ExecutionTracer


# 可选插件

* ModuleTracer (_用于调试信息_)



